
const    db      = require('../../modelos/bd')
const    Product = require('../../modelos/Productos/productos')



//Listar todos los productos
exports.listarProductos = (req, res, next) => {
  // res.send('Listado de Productos : desde el controlador')
  // esto se deshabilita para poder ver el json si no sale error

  db.query(Product.verProductos(), (err, data) => {
    
    if (err) throw err
    else {
      res.status(200).json({
        message: "Listado de Productos.",
        productId: data
      })
    }
    
  })
}

//crear un Producto nuevo
exports.crearProducto = (req, res, next) => {
  // res.send('Insertar Producto :) : desde el controlador')
  //read product information from request
  let productoNuevo = new Product(req.body.PRD_NAME, req.body.PRD_PRICE)
  // ojo aqui los campos tienen que ser exactamente los que estan en la tabla si estan en 
  // mayusculas tienen que estar en mayusculas tal como los compos estan en la tabla
  console.log(req.body.PRD_NAME, req.body.PRD_PRICE)
  console.log(productoNuevo)

  db.query(productoNuevo.insertarProducto(), (err, data) => {
    console.log(data)
    res.status(200).json({
      message: "Producto adicionado.",
      productId: data.insertId
    })
  })
}

// Mostrar un producto
exports.editarProducto = (req, res, next) => {
  let pid = req.params.id
  console.log(pid)
  db.query(Product.verProducto(pid), (err, data) => {
    if (!err) {
      if (data && data.length > 0) {

        res.status(200).json({
          message: "Producto encontrado.",
          product: data
        });
      } else {
        res.status(200).json({
          message: "Producto no encontrado."
        })
      }
    }
  })
}

// modificar un producto
exports.modificarProducto = (req, res, next) => {
  let cambiar = new Product(req.body.PRD_NAME, req.body.PRD_PRICE)
  pid = req.params.id
  // console.log(req.body)
  db.query(cambiar.modificarProducto(pid), (err, data) => {
    if (!err) {
      if (data && data.affectedRows > 0) {
        res.status(200).json({
          message: `Producto modificado con id = ${pid}.`,
          affectedRows: data.affectedRows
        })
      } else {
        res.status(200).json({
          message: "Producto no encontrado."
        })
      }
    } else {
      console.log(err)
    }
  })
}

//Eliminar un producto
exports.eliminarProducto = (req, res, next) => {

  //var pid = req.body.productId
  pid = req.params.id
  
  db.query(Product.eliminarProducto(pid), (err, data) => {
    if (!err) {
      if (data && data.affectedRows > 0) {
        res.status(200).json({
          message: `Product deleted with id = ${pid}.`,
          affectedRows: data.affectedRows
        })
      } else {
        res.status(200).json({
          message: "Product Not found."
        })
      }
    } else {
      console.log(err)
    }
  })
}

