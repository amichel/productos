const   express     = require('express')
        morgan      = require('morgan')
        bodyParser  = require('body-parser')
        cors        = require('cors')
        app         = express()
        port        = process.env.PORT | 8081

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// Rutas en el servidor
app.use(require('../rutas/'))  // por defecto toma el directorio /rutas/index.js


app.listen(port,()=>{
    console.log('Escuchando en el puerto 8081')
})