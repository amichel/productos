'use strict';
const express = require('express')
const rutaProductos = express.Router()
const Productos = require('../../controladores/Productos/controllerProducto')

rutaProductos.route('/')
  .get(Productos.listarProductos)
  .post(Productos.crearProducto)

rutaProductos.route('/:id')
   .get(Productos.editarProducto)
   .put(Productos.modificarProducto)
   .delete(Productos.eliminarProducto)

module.exports = rutaProductos