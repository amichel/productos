const express = require('express');
const routeHome = express.Router();

routeHome.get('/', function (req, res) {
  res.send(`<h1>Hello World by Armando Michel</h1>`);
});

module.exports = routeHome;
