const   express = require('express')
        router  = express.Router()

home = require('./routeHome')
productos = require('./Productos/rutaProductos')


/**
  * @description
  * Manejo de Html Estatico /, home
  * Despues todas las rutas Dinamicas / productos
*/

router.use('/', home)
router.use('/productos', productos)


// caso contrario error
router.get('*', (req, res) => {
  res.send('No implementado por Armando Michel Akamine aun')
})

module.exports = router
