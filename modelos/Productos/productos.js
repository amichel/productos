class Producto {

  constructor(nombre, precio) {
    this.prd_id = 0;
    this.prd_name = nombre;
    this.prd_price = precio;
  }

  insertarProducto() {
    let sql = `INSERT INTO product(prd_name, prd_price) \
                   VALUES('${this.prd_name}',${this.prd_price})`;
    return sql;
  }

  static verProducto(prd_id) {
    let sql = `SELECT * FROM product WHERE PRD_ID = ${prd_id}`;
    return sql;
  }

  modificarProducto(prd_id) {
    console.log(this.prd_name)
    
    let sql = `UPDATE product set PRD_NAME='${this.prd_name}', PRD_PRICE = ${this.prd_price} WHERE PRD_ID = ${prd_id}`;
    return sql;
  }

  static eliminarProducto(prd_id) {
    let sql = `DELETE FROM product WHERE PRD_ID = ${prd_id}`;
    return sql;
  }

  static verProductos() {
    let sql = `SELECT * FROM product`;
    return sql;
  }
}

module.exports = Producto;